
/*
* Change Key control values From AN12196
SesAuthMAC = Buffer.from('5529860B2FC5FB6154B7F28361D30BF9', 'hex');
SesAuthENC = Buffer.from('4CF3CB41A22583A61E89B158D252FC53', 'hex');
TI = Buffer.from('7614281A', 'hex');
masterKey = Buffer.from('C8EE97FD8B00185EDC7598D7FEBC818A', 'hex');
newKey = Buffer.from('F3847D627727ED3BC9C4CC050489B966', 'hex');
cmdCtr = 2;
keyNo = 2;

const control = {
  xor: Buffer.from('F3847D627727ED3BC9C4CC050489B966', 'hex'),
  crc32: Buffer.from('789DFADC', 'hex'),
  IV: Buffer.from('A55A7614281A02000000000000000000', 'hex'),
  IVe: Buffer.from('307EDE1814707F30CFE603DD6CA62353', 'hex'),
  keyData: Buffer.from('F3847D627727ED3BC9C4CC050489B96601789DFADC8000000000000000000000', 'hex'),
  keyDataE: Buffer.from('2CF362B7BF4311FF3BE1DAA295E8C68DE09050560D19B9E16C2393AE9CD1FAC7', 'hex'),
  CMACinput: Buffer.from('C402007614281A022CF362B7BF4311FF3BE1DAA295E8C68DE09050560D19B9E16C2393AE9CD1FAC7', 'hex'),
  CMAC: Buffer.from('EA5D2E0CBFE24C0BCBCD501D21060EE6', 'hex'),
  CMACt: Buffer.from('5D0CE20BCD1D06E6', 'hex'),
  payload: Buffer.from('90C4000029022CF362B7BF4311FF3BE1DAA295E8C68DE09050560D19B9E16C2393AE9CD1FAC75D0CE20BCD1D06E600', 'hex'),
}
*/

/*
// ChangeFileSettings Test values from application notes pdf
SesAuthMAC = Buffer.from('4C6626F5E72EA694202139295C7A7FC7', 'hex');
SesAuthENC = Buffer.from('1309C877509E5A215007FF0ED19CA564', 'hex');
TI = Buffer.from('9D00C4DF', 'hex');
cmdData = Buffer.from('4000E0C1F121200000430000430000', 'hex');
cmdCtr = 1;

control: {
  mac: "7B D7 5F 99 1C B7 A2 C1 8D A0 9E EF 04 7A 8D 04".split(" ").join("").toLowerCase(),
  mact: "D7 99 B7 C1 A0 EF 7A 04".split(" ").join("").toLowerCase(),
  settings: "02 61B6D97903566E84C3AE5274467E89EA D799B7C1A0EF7A04".split(" ").join("").toLowerCase()
},

*/
